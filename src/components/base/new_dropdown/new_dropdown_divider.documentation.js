import description from './new_dropdown_divider.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-divider',
};
