export const tokenList = [
  'giraffe',
  'dog',
  'dodo',
  'komodo dragon',
  'hippo',
  'platypus',
  'jackalope',
  'quezal',
  'badger',
  'vicuña',
  'whale',
  'xenarthra',
];

export const labelText = 'Animals We Tolerate';
