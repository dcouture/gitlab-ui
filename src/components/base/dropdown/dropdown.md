# Dropdown

> Note: This component is being migrated to the `GlNewDropdown` component as it conforms to the
> [design specs](https://design.gitlab.com/components/dropdowns#dropdown). Please use
> `GlNewDropdown` instead. You can read more about the migration
> [here](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/673).

<!-- STORY -->

## Usage

The dropdown component offers a user multiple items or actions to choose from which are initially collapsed behind a button.
