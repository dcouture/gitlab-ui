# [17.26.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.25.0...v17.26.0) (2020-07-14)


### Features

* **GlFilteredSearch:** Add back white background ([5b511e7](https://gitlab.com/gitlab-org/gitlab-ui/commit/5b511e75233869766db6c068d03c5054ab245417))

# [17.25.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.24.0...v17.25.0) (2020-07-13)


### Features

* **GlCombobox:** Add GlCombobox component ([618f774](https://gitlab.com/gitlab-org/gitlab-ui/commit/618f774b47ecfca30a4687ad55ab96fb152ecd8d))

# [17.24.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.23.0...v17.24.0) (2020-07-13)


### Features

* add gl-rounded-top-base util class ([0ced5bb](https://gitlab.com/gitlab-org/gitlab-ui/commit/0ced5bb714c23965219f22d084cd2ec7ea46c2a0))

# [17.23.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.22.1...v17.23.0) (2020-07-13)


### Features

* **GlTruncate:** add truncate component ([3839080](https://gitlab.com/gitlab-org/gitlab-ui/commit/383908013f0141f52c66754bfee9222d66ae1e65))

## [17.22.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.22.0...v17.22.1) (2020-07-13)


### Bug Fixes

* **GlLegend:** Show dash on multiple empty cases ([30c1d16](https://gitlab.com/gitlab-org/gitlab-ui/commit/30c1d16ef7f7d9089ce5960ddaecc3647d4a2ca5))

# [17.22.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.21.0...v17.22.0) (2020-07-12)


### Features

* Add mt-5 and align-self-center for sm ([8d2a99c](https://gitlab.com/gitlab-org/gitlab-ui/commit/8d2a99c2081518f11249fce6a4d334356a72ad84))

# [17.21.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.20.0...v17.21.0) (2020-07-10)


### Features

* **css:** adding new background utility mixin for data-viz-orange-800 ([ecc82a9](https://gitlab.com/gitlab-org/gitlab-ui/commit/ecc82a92470314ed6b7fe1daa9864423fbdac341))

# [17.20.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.19.2...v17.20.0) (2020-07-10)


### Features

* **GlFilteredSearch:** Style disabled state ([314daf8](https://gitlab.com/gitlab-org/gitlab-ui/commit/314daf87855c4549e4944acc8f3dd1fc3de6351e))

## [17.19.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.19.1...v17.19.2) (2020-07-09)


### Bug Fixes

* **search-box-by-type:** Fix clear button focus ([4172c34](https://gitlab.com/gitlab-org/gitlab-ui/commit/4172c34ecbcf0dabd4975579ebd80184b1660c33))

## [17.19.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.19.0...v17.19.1) (2020-07-09)


### Bug Fixes

* **GlTokenSelector:** fix padding override ([de6970b](https://gitlab.com/gitlab-org/gitlab-ui/commit/de6970b248a2e40638ec3d1f1367d5f339c7e30b))

# [17.19.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.18.1...v17.19.0) (2020-07-08)


### Features

* add missing spacing utils from 1-7 ([8023560](https://gitlab.com/gitlab-org/gitlab-ui/commit/8023560fec571efef25a25e0a0e327f9ed34d6ac))

## [17.18.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.18.0...v17.18.1) (2020-07-07)


### Bug Fixes

* **GlGroup:** Update GLRadio and GlCheck Groups ([28de43d](https://gitlab.com/gitlab-org/gitlab-ui/commit/28de43dfad26dcdcf209ea6ea58e566068636802))

# [17.18.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.17.0...v17.18.0) (2020-07-07)


### Features

* Add gl-sticky utility class ([d2fcd6a](https://gitlab.com/gitlab-org/gitlab-ui/commit/d2fcd6a995641ab5d33eadc4720d7aaaca731bb3))

# [17.17.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.16.0...v17.17.0) (2020-07-06)


### Features

* **css:** Add absolute position utility classes ([976e498](https://gitlab.com/gitlab-org/gitlab-ui/commit/976e498349eb373f0ecdfd815fa172527ddd2b36))
* **spinner:** Remove hardcoded aria-hidden ([f50816c](https://gitlab.com/gitlab-org/gitlab-ui/commit/f50816ce8a851c5da1cae3d86f2a63664421aadb))
* Add gl-ml-11 utility class ([05be542](https://gitlab.com/gitlab-org/gitlab-ui/commit/05be542ea7eeba35f63e63fccfb4c7c6ad49ffe8))

# [17.16.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.15.0...v17.16.0) (2020-07-06)


### Features

* add gl-text-body mixin for default text color ([63dec63](https://gitlab.com/gitlab-org/gitlab-ui/commit/63dec63c00ad0a207416d9578aa0acd97fe2162c))

# [17.15.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.14.0...v17.15.0) (2020-07-06)


### Features

* **css:** Add size min-width full to utility class ([9e91352](https://gitlab.com/gitlab-org/gitlab-ui/commit/9e9135272a7e489019deb3772146280e7d82069f))

# [17.14.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.13.0...v17.14.0) (2020-07-03)


### Features

* **css:** Add responsive margin to utility class ([3b571d0](https://gitlab.com/gitlab-org/gitlab-ui/commit/3b571d04c27926d9643418dddeff555c68138f82))

# [17.13.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.12.0...v17.13.0) (2020-07-02)


### Features

* add 2px top and bottom borders ([7be09e8](https://gitlab.com/gitlab-org/gitlab-ui/commit/7be09e8e784bdd54763ffe1b13fd80ab5c382a9d))

# [17.12.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.11.0...v17.12.0) (2020-07-02)


### Features

* **filtered-search:** Add container style support for token ([6bc9b02](https://gitlab.com/gitlab-org/gitlab-ui/commit/6bc9b020364e46be86f3f2c02e06a5e67a514935))

# [17.11.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.10.1...v17.11.0) (2020-07-02)


### Features

* **textarea:** Allow resizing ([b0b8ece](https://gitlab.com/gitlab-org/gitlab-ui/commit/b0b8ece00d0dac909aa4329ae107c27e5555ea0b))

## [17.10.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.10.0...v17.10.1) (2020-06-30)


### Bug Fixes

* fix invisible borders in Edge and friends ([39a4d89](https://gitlab.com/gitlab-org/gitlab-ui/commit/39a4d89604254e0df60a2ea8db52f4fe9b47ba22))

# [17.10.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.9.0...v17.10.0) (2020-06-30)


### Features

* **css:** Add size 12 width utility class ([c90dde7](https://gitlab.com/gitlab-org/gitlab-ui/commit/c90dde7ec8e97857af0055b8b4e4960059bc1643))

# [17.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.8.2...v17.9.0) (2020-06-29)


### Features

* add gl-text-truncate utility class ([2eefff9](https://gitlab.com/gitlab-org/gitlab-ui/commit/2eefff991ced0aa0122efe3e78ea422204d374ae))

## [17.8.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.8.1...v17.8.2) (2020-06-26)


### Bug Fixes

* **buttons:** Correct default button resting border color ([1e8305a](https://gitlab.com/gitlab-org/gitlab-ui/commit/1e8305ada8bf352ca44c0f7c0d5cf085e6f548ed))

## [17.8.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.8.0...v17.8.1) (2020-06-26)


### Bug Fixes

* **toggle:** Add help text to toggles ([2ba188a](https://gitlab.com/gitlab-org/gitlab-ui/commit/2ba188aa1b31cb8fef1833a28a1f49f76a780a6d))

# [17.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.7.1...v17.8.0) (2020-06-26)


### Features

* Make card bg color overridable with scss ([39cd1d8](https://gitlab.com/gitlab-org/gitlab-ui/commit/39cd1d87d13b275f3a101140dafc89e6647c9527))

## [17.7.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.7.0...v17.7.1) (2020-06-26)


### Bug Fixes

* **search-box-by-type:** Revert clear button focus fix ([a4b17f0](https://gitlab.com/gitlab-org/gitlab-ui/commit/a4b17f01449e37b704c9da18b97adf7bb732e8a7))

# [17.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.6.2...v17.7.0) (2020-06-26)


### Features

* add gl-pt-2 utility class ([2e48434](https://gitlab.com/gitlab-org/gitlab-ui/commit/2e4843483df5b3cac19105f09d018a44f81a3b44))

## [17.6.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.6.1...v17.6.2) (2020-06-26)


### Bug Fixes

* **search-box-by-type:** Fix clear button focus ([89d2697](https://gitlab.com/gitlab-org/gitlab-ui/commit/89d26970678a8ce8502add4b7ab2228df8200622))

## [17.6.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.6.0...v17.6.1) (2020-06-25)


### Bug Fixes

* **tooltip:** Remove x pad for topright positions ([cf1c5b8](https://gitlab.com/gitlab-org/gitlab-ui/commit/cf1c5b80aab72a20a47e9539631d78da40058d13))

# [17.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.5.0...v17.6.0) (2020-06-24)


### Features

* **GlTokenSelector:** explicitly emit `keydown` event ([d9f222d](https://gitlab.com/gitlab-org/gitlab-ui/commit/d9f222da7126dd3cfa38b1ce9b5f02c4fd0f99e6))

# [17.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.4.0...v17.5.0) (2020-06-24)


### Features

* **chart:** Reverse order of tooltip values ([83a2302](https://gitlab.com/gitlab-org/gitlab-ui/commit/83a23021e97a7fca6baa09d77d6b77a64fccdf06))

# [17.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.3.1...v17.4.0) (2020-06-24)


### Features

* **modal:** Modal with scrollable content ([dc03c44](https://gitlab.com/gitlab-org/gitlab-ui/commit/dc03c44e24398a3be91b6b2c26726c890d57381c))

## [17.3.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.3.0...v17.3.1) (2020-06-23)


### Bug Fixes

* **GLTokenSelector:** move text input specific HTML attributes ([59d2bb9](https://gitlab.com/gitlab-org/gitlab-ui/commit/59d2bb9bf69cfb15d897f9621db1f4fcc868432f))

# [17.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.2.0...v17.3.0) (2020-06-23)


### Features

* **charts:** Add legend hover state ([65002eb](https://gitlab.com/gitlab-org/gitlab-ui/commit/65002ebf3edbb5451c0de19f0bd599a1920635f2))

# [17.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.1.0...v17.2.0) (2020-06-19)


### Features

* Add gl-mb-7 utlity class ([f0ff828](https://gitlab.com/gitlab-org/gitlab-ui/commit/f0ff8286f30b8a4615306d055deb52ce4d54b1e0))

# [17.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.0.1...v17.1.0) (2020-06-18)


### Features

* Add gl-display-sm-none and gl-translate-y-n100 classes ([506e519](https://gitlab.com/gitlab-org/gitlab-ui/commit/506e5196e6aedec82264f3f1a860efbed09901b9))

## [17.0.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v17.0.0...v17.0.1) (2020-06-17)


### Bug Fixes

* Revert Set modal max-height dynamically ([1f9bb02](https://gitlab.com/gitlab-org/gitlab-ui/commit/1f9bb02df105b6a102043b96101e07c82787de45))

# [17.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.12.2...v17.0.0) (2020-06-17)


### Bug Fixes

* **filters:** Properly work with history dropdown items ([c8d9e31](https://gitlab.com/gitlab-org/gitlab-ui/commit/c8d9e31a03fb577112b4075f696434d57e41577c))


### BREAKING CHANGES

* **filters:** Selecting history item does not trigger
submit event anymore. Instead new history-item-selected
event will be triggered.

This allows customizing behavior of our search when needed

## [16.12.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.12.1...v16.12.2) (2020-06-16)


### Bug Fixes

* **GlButton:** Increse GlButton CSS specificity ([0d22860](https://gitlab.com/gitlab-org/gitlab-ui/commit/0d228603fde0987cc754b795a10781687ffd8795))
* **GlSafeLinkDirective:** Make update transformation reactive ([05565ad](https://gitlab.com/gitlab-org/gitlab-ui/commit/05565ad53de0aa38c897e7bfcc1ff0ee0f815364))

## [16.12.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.12.0...v16.12.1) (2020-06-16)


### Reverts

* Revert "fix(toggles): Add help text to toggles" ([ad5c55d](https://gitlab.com/gitlab-org/gitlab-ui/commit/ad5c55d006987a86325df733071ad13922a1c6a6))

# [16.12.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.11.0...v16.12.0) (2020-06-16)


### Features

* **css:** Add negative margin utilities ([393581c](https://gitlab.com/gitlab-org/gitlab-ui/commit/393581c9c5ff0777e60a2ff33e2a2ae2c0de0d33))
* **modal:** Set modal max height dynamically ([58470b5](https://gitlab.com/gitlab-org/gitlab-ui/commit/58470b5ee0c9afb495a99894c4b73b60d1119524))

# [16.11.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.10.0...v16.11.0) (2020-06-15)


### Bug Fixes

* **toggles:** Add help text to toggles ([e3a93a4](https://gitlab.com/gitlab-org/gitlab-ui/commit/e3a93a44e20d04f3321b97b440999b63bef4e0fc))


### Features

* **GlLink:** Add safe-link directive to gl-link ([31817ef](https://gitlab.com/gitlab-org/gitlab-ui/commit/31817ef4e1c9a2f45f0bfe9b17a59a3162ef12eb))

# [16.10.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.9.0...v16.10.0) (2020-06-15)


### Features

* **GlTokenSelector:** Add token selector component ([6a55777](https://gitlab.com/gitlab-org/gitlab-ui/commit/6a5577703a0c8c572310b4fc92b06ccdcb3c190e))

# [16.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.8.3...v16.9.0) (2020-06-12)


### Features

* **css:** Add padding utility ([192ff26](https://gitlab.com/gitlab-org/gitlab-ui/commit/192ff26d76902805aa29dc7513b1223bfe001e75))

## [16.8.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.8.2...v16.8.3) (2020-06-12)


### Bug Fixes

* **button:** Remove font weight bold from primary buttons ([2e7549d](https://gitlab.com/gitlab-org/gitlab-ui/commit/2e7549d15d3bf84e9e1bacee8e8ec137aa36b5d1))

## [16.8.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.8.1...v16.8.2) (2020-06-11)


### Bug Fixes

* **GlChartLegend:** remove tooltips from labels of tabular legends ([8e9b7bd](https://gitlab.com/gitlab-org/gitlab-ui/commit/8e9b7bdd5e5650c6f2b5ba8387794de65b52e70f))

## [16.8.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.8.0...v16.8.1) (2020-06-11)


### Bug Fixes

* **GlHeatMap:** Enable responsive heatmap ([a490d62](https://gitlab.com/gitlab-org/gitlab-ui/commit/a490d621f4e5fd7ba7b316d4390cb88b4345ca46))

# [16.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.7.0...v16.8.0) (2020-06-11)


### Features

* **utilities:** Add gl-str-truncated ([1424ba9](https://gitlab.com/gitlab-org/gitlab-ui/commit/1424ba99a8682369a0eb09409e09e3d5d3840288))

# [16.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.6.3...v16.7.0) (2020-06-11)


### Features

* **Alert:** normalize text, buttons, and links ([495179e](https://gitlab.com/gitlab-org/gitlab-ui/commit/495179e84ebd68bd0053a5799b3ebd7ae9c80e4e))

## [16.6.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.6.2...v16.6.3) (2020-06-10)


### Bug Fixes

* **GlHeatMap:** disable pointer events on legend ([c7263da](https://gitlab.com/gitlab-org/gitlab-ui/commit/c7263daa5481381f5bfff2b3e98b48d3f72aa01b))

## [16.6.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.6.1...v16.6.2) (2020-06-10)


### Bug Fixes

* **css:** Add "deg" unit to rotation utilities ([d10c025](https://gitlab.com/gitlab-org/gitlab-ui/commit/d10c025107d0cd118678162e5519241dfb8e6459))

## [16.6.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.6.0...v16.6.1) (2020-06-10)


### Bug Fixes

* **GlLabel:** Add focus state ([12f43ea](https://gitlab.com/gitlab-org/gitlab-ui/commit/12f43eae2e58a3a805afd04d295e9592aa177164))

# [16.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.5.0...v16.6.0) (2020-06-10)


### Features

* **GlSafeLink:** Add safe link directive ([1721feb](https://gitlab.com/gitlab-org/gitlab-ui/commit/1721feb893c037ac7eecd72ec215e9219858d04b))

# [16.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.4.1...v16.5.0) (2020-06-09)


### Features

* **css:** Add rotate and flip utilities ([aeb1c21](https://gitlab.com/gitlab-org/gitlab-ui/commit/aeb1c213332b7e2f6064fa4843bf17150387125e))

## [16.4.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.4.0...v16.4.1) (2020-06-09)


### Bug Fixes

* **css:** Update segment control css ([09a3875](https://gitlab.com/gitlab-org/gitlab-ui/commit/09a3875a86845003784aaab38d52178a4e2a945a))
* **GlHeatMap:** Update broken styles ([cba66e8](https://gitlab.com/gitlab-org/gitlab-ui/commit/cba66e83664188dfbfd5821831eed025697e426f))

# [16.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.3.0...v16.4.0) (2020-06-09)


### Features

* **css:** Added gl-my-3 utility ([5d2f9cc](https://gitlab.com/gitlab-org/gitlab-ui/commit/5d2f9cc319789fccdb44fae9fbcbb561334f2116))

# [16.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.2.1...v16.3.0) (2020-06-05)


### Features

* **AreaChart, LineChart:** prevent data tooltip freeze ([78d6a17](https://gitlab.com/gitlab-org/gitlab-ui/commit/78d6a17c6eccc237a3298e3d1c80ed29be37073b))

## [16.2.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.2.0...v16.2.1) (2020-06-03)


### Bug Fixes

* **deps:** upgrade BootstrapVue to v2.13.1 ([58b2711](https://gitlab.com/gitlab-org/gitlab-ui/commit/58b2711a80338522ad734b0990a8a59b80a83401))

# [16.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.1.0...v16.2.0) (2020-06-03)


### Features

* **css:** Add more css utils ([21f8403](https://gitlab.com/gitlab-org/gitlab-ui/commit/21f8403e9695aeab89cbb7fe9203e72e1208daa6))

# [16.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v16.0.0...v16.1.0) (2020-05-28)


### Features

* Add .gl-sr-only utility class ([0d64fdd](https://gitlab.com/gitlab-org/gitlab-ui/commit/0d64fddc0c38de40eae4f33163eaa9b916a9d87c))

# [16.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.6.0...v16.0.0) (2020-05-26)


### Features

* **badge:** Update styling of text badges ([ece3e44](https://gitlab.com/gitlab-org/gitlab-ui/commit/ece3e44194a54ae8ab2ebe2cd5e172d5ec6396c6))


### BREAKING CHANGES

* **badge:** This implements the [text-only][1] versions of the
latest badge designs.  Support for icons will be added in a future
version.

 - This removes the `primary`, `secondary`, `light` and `dark` variants
   of GlBadge, and adds the `muted` and `neutral` variants.
 - The `pill` prop is now ignored, and all GlBadges are of the `pill`
   type.

It's recommended to replace usage of the removed variants with the new
variants:

    primary   -> info
    secondary -> neutral
    light     -> muted
    dark      -> neutral

[1]: https://gitlab.com/gitlab-org/gitlab-ui/-/issues/481

# [15.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.5.0...v15.6.0) (2020-05-25)


### Features

* **css:** Mark utility classes as !important ([c4d7a4e](https://gitlab.com/gitlab-org/gitlab-ui/commit/c4d7a4e6d7f295f7b74a200477a21753526e2cf9))

# [15.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.4.3...v15.5.0) (2020-05-25)


### Features

* **css:** Add overflow visible utility ([1844a1b](https://gitlab.com/gitlab-org/gitlab-ui/commit/1844a1b32c8cb1820e42fc77b166fb217bedca91))

## [15.4.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.4.2...v15.4.3) (2020-05-25)


### Bug Fixes

* **infinite-scoll:** Render totalItems if positive ([cc3f75e](https://gitlab.com/gitlab-org/gitlab-ui/commit/cc3f75ee50def8226adf23f40384f121b5cbae46))

## [15.4.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.4.1...v15.4.2) (2020-05-25)


### Bug Fixes

* **css:** gl-bg-data-viz-aqua typo ([e710541](https://gitlab.com/gitlab-org/gitlab-ui/commit/e71054171846ab8b9cd7e600b42a544ca168862f))

## [15.4.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.4.0...v15.4.1) (2020-05-23)


### Bug Fixes

* include src dir in npm package ([9402519](https://gitlab.com/gitlab-org/gitlab-ui/commit/9402519c0ea562d0bd191f500a0dd9b5edd7b260))

# [15.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.3.0...v15.4.0) (2020-05-22)


### Features

* **css:** Add overflow scroll utility ([3c961fd](https://gitlab.com/gitlab-org/gitlab-ui/commit/3c961fdfc245c03ed897aa05930dbc11c5f977b9))

# [15.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.2.0...v15.3.0) (2020-05-22)


### Features

* **gl-ml-*:** Add missing `gl-ml-*` utility mixins ([3711c01](https://gitlab.com/gitlab-org/gitlab-ui/commit/3711c0185f6225d611e8eedad46f5c8f50226bfd))

# [15.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.1.0...v15.2.0) (2020-05-22)


### Features

* **css:** Add padding-top-9 mixin ([2da3da5](https://gitlab.com/gitlab-org/gitlab-ui/commit/2da3da5dc845063367a8f489e6b3b3d622591744))

# [15.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v15.0.0...v15.1.0) (2020-05-21)


### Features

* Added font-style: italic utility ([9de0259](https://gitlab.com/gitlab-org/gitlab-ui/commit/9de02597a26eb20e488d2522b75f8d37472f9a46))

# [15.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.17.0...v15.0.0) (2020-05-21)


### Bug Fixes

* **css:** update stateful and responsive mixins naming pattern ([d2e99f5](https://gitlab.com/gitlab-org/gitlab-ui/commit/d2e99f587afc451639432376f59f543508c169f4))


### BREAKING CHANGES

* **css:** Stateful and responsive mixins naming patterns
have been updated so that they always start with the `gl-` prefix,
followed by the state or breakpoint.

Make sure to update your utility class usages to reflect those changes.

For example:

```patch
- <div class="hover-gl-rounded-base">
+ <div class="gl-hover-rounded-base">
```

# [14.17.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.16.0...v14.17.0) (2020-05-21)


### Features

* **markdown:** Compact markdown typescale ([04702d7](https://gitlab.com/gitlab-org/gitlab-ui/commit/04702d7f0f2dc62a29dcf257bc25d2aac69809e4))

# [14.16.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.15.0...v14.16.0) (2020-05-20)


### Features

* **css:** compile utility classes library ([a0fa474](https://gitlab.com/gitlab-org/gitlab-ui/commit/a0fa47459a91fb9135fc820962d7c0745d32720e))
* **css:** Harmonize Bootstrap and GitLab UI bolds ([d811d66](https://gitlab.com/gitlab-org/gitlab-ui/commit/d811d669a717fad6061b9b230af361d320ffac69))

# [14.15.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.14.2...v14.15.0) (2020-05-20)


### Features

* **css:** add stateful utility classes ([5841a1d](https://gitlab.com/gitlab-org/gitlab-ui/commit/5841a1dc3611c5a93818b031d714c68c9ad1b0ea))

## [14.14.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.14.1...v14.14.2) (2020-05-20)


### Bug Fixes

* **GlLabel:** Add focus state to label ([aa7d8b9](https://gitlab.com/gitlab-org/gitlab-ui/commit/aa7d8b9fb5928706a6cc0f0f9c83046e9d03b7e7))

## [14.14.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.14.0...v14.14.1) (2020-05-20)


### Bug Fixes

* **GlLegend:** Remove bogus test selector ([4982611](https://gitlab.com/gitlab-org/gitlab-ui/commit/49826119b925accfee445540bb63d4648942775d))

# [14.14.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.13.1...v14.14.0) (2020-05-20)


### Features

* **GlDropdown:** Add show method to GlDropdown ([7e95b05](https://gitlab.com/gitlab-org/gitlab-ui/commit/7e95b055036027c623f73cadc33235e0afec606b))


### Reverts

* fix(Link): Add support to allow only safe urls ([1bb51a0](https://gitlab.com/gitlab-org/gitlab-ui/commit/1bb51a02eb21af3da3b553b42d9346f87998ccdb))

## [14.13.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.13.0...v14.13.1) (2020-05-19)


### Bug Fixes

* **Link:** Add support to allow only safe urls ([85b2ae1](https://gitlab.com/gitlab-org/gitlab-ui/commit/85b2ae1f86bb59bc2639d90b87e2c1e3cea73bb4))

# [14.13.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.12.1...v14.13.0) (2020-05-18)


### Features

* **css:** Expose gl-border-r-2 class ([6600c9a](https://gitlab.com/gitlab-org/gitlab-ui/commit/6600c9a3bb454bf52aeb1a78b8708d9673e56ce5))
* **css:** Expose some data viz color utility classes ([3615162](https://gitlab.com/gitlab-org/gitlab-ui/commit/3615162312700efd4965444f246a197754f3097c))

## [14.12.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.12.0...v14.12.1) (2020-05-18)


### Bug Fixes

* **GlLegend:** Add stable test selector ([c8f2d48](https://gitlab.com/gitlab-org/gitlab-ui/commit/c8f2d4873a583cd842aff722ebd40a6aaf9ee436))

# [14.12.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.11.2...v14.12.0) (2020-05-18)


### Features

* **css:** Expose gl-h-2 utility class ([2300ab6](https://gitlab.com/gitlab-org/gitlab-ui/commit/2300ab68ac58dbb08a10aabc12841b47d9bbfca2))

## [14.11.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.11.1...v14.11.2) (2020-05-18)


### Bug Fixes

* **label:** Make dark scoped label text compatible with dark theme ([ec88fc0](https://gitlab.com/gitlab-org/gitlab-ui/commit/ec88fc09b5e62a6158552995fe3de95241f21d96))

## [14.11.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.11.0...v14.11.1) (2020-05-15)


### Bug Fixes

* **GlDatepicker:** fix x-axis padding ([9d4ed9f](https://gitlab.com/gitlab-org/gitlab-ui/commit/9d4ed9f2e65b139a8d6393583a2dd99f4f4676a0))

# [14.11.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.10.2...v14.11.0) (2020-05-15)


### Features

* **charts:** add tabular legend layout support to charts ([512d6b3](https://gitlab.com/gitlab-org/gitlab-ui/commit/512d6b3cd203c4495b7ee17b3805f1ffca52f10a))

## [14.10.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.10.1...v14.10.2) (2020-05-15)


### Bug Fixes

* uses the new buttons in empty states ([6f754ff](https://gitlab.com/gitlab-org/gitlab-ui/commit/6f754ff948e1b491b5ea9a32a8daf317d66d75e5))

## [14.10.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.10.0...v14.10.1) (2020-05-14)


### Bug Fixes

* **GlTextArea:** Fix form input readonly styles ([7f1e9a2](https://gitlab.com/gitlab-org/gitlab-ui/commit/7f1e9a28d3214bd2f75d905a2154370c76d49b0c))

# [14.10.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.9.0...v14.10.0) (2020-05-14)


### Features

* **css:** Add flex-direction-row responsive utilities ([9460fc5](https://gitlab.com/gitlab-org/gitlab-ui/commit/9460fc5edde5629c3013685acb21420ea025d012))

# [14.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.8.1...v14.9.0) (2020-05-14)


### Features

* **nav:** Add GlNavItemDropdown component ([c011b64](https://gitlab.com/gitlab-org/gitlab-ui/commit/c011b64fc84cdd753b41a9da91604cf7b3ce5326))
* **nav:** Add GlNavItemDropdown component ([21adf07](https://gitlab.com/gitlab-org/gitlab-ui/commit/21adf073b620b627215ccbab7df418273e16b18e))

## [14.8.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.8.0...v14.8.1) (2020-05-13)


### Bug Fixes

* **GlFormGroup:** Remove border-bottom ([d212ea9](https://gitlab.com/gitlab-org/gitlab-ui/commit/d212ea93137b3233771a587ce54e591921bfc3b1))

# [14.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.7.0...v14.8.0) (2020-05-12)


### Features

* **css:** Add border utilties ([b7f0a45](https://gitlab.com/gitlab-org/gitlab-ui/commit/b7f0a45640da4873cb2ff998adf5341120ab891b))

# [14.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.6.1...v14.7.0) (2020-05-12)


### Bug Fixes

* **GlFormInput:** Restore form input plaintext styles ([b270072](https://gitlab.com/gitlab-org/gitlab-ui/commit/b270072fa5d9ffdce91d9d93d089fd00e400ea13))


### Features

* **GlChartLegend:** add tabular layout variation ([8180bf2](https://gitlab.com/gitlab-org/gitlab-ui/commit/8180bf23796eb4b83e4aaefb366638e6ef98fd64))

## [14.6.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.6.0...v14.6.1) (2020-05-11)


### Bug Fixes

* **css:** add gray-800 text utility ([f2d85ee](https://gitlab.com/gitlab-org/gitlab-ui/commit/f2d85eef414f785d80c26958e65367fcf21a4bb7))

# [14.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.5.2...v14.6.0) (2020-05-11)


### Features

* **css:** add two padding classe ([c4c3969](https://gitlab.com/gitlab-org/gitlab-ui/commit/c4c39699d378b1cfa3ae610e78347a5e4814ab3d))

## [14.5.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.5.1...v14.5.2) (2020-05-11)


### Bug Fixes

* **GlButton:** Add conditional for icon ([ca2d5ee](https://gitlab.com/gitlab-org/gitlab-ui/commit/ca2d5ee57bc2005d2079689284f4535dddabc12a))

## [14.5.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.5.0...v14.5.1) (2020-05-11)


### Bug Fixes

* **css:** Rename inherit color to reset ([d8ec0fc](https://gitlab.com/gitlab-org/gitlab-ui/commit/d8ec0fcc2131ea9723c670eee592ed329f4853f7))
* **Path:** Fix styles on non-white bg ([e070796](https://gitlab.com/gitlab-org/gitlab-ui/commit/e0707968f8b5f1c5974901b1e5fdca45b4c87206))

# [14.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.4.1...v14.5.0) (2020-05-07)


### Bug Fixes

* **charts:** Use correct palette for series ([29d43cb](https://gitlab.com/gitlab-org/gitlab-ui/commit/29d43cb6411f966c4ec021d8b03776314c0bca84))


### Features

* Add additional justify css styles ([d7ea9fd](https://gitlab.com/gitlab-org/gitlab-ui/commit/d7ea9fd289121c7e3b65fd2b4e730e7735944507))

## [14.4.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.4.0...v14.4.1) (2020-05-07)


### Bug Fixes

* **css:** Rename inherist text to reset ([36c335e](https://gitlab.com/gitlab-org/gitlab-ui/commit/36c335e3c974419f045201988a10da117bfec25a))

# [14.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.3.1...v14.4.0) (2020-05-07)


### Bug Fixes

* Annotations tooltip logic ([fafe255](https://gitlab.com/gitlab-org/gitlab-ui/commit/fafe2555264b62b92c3eee6686225038458a2d88))


### Features

* **Path:** Add icon to Path component ([b09443f](https://gitlab.com/gitlab-org/gitlab-ui/commit/b09443f4a665875399eb4041008c8e2faee7ff75))

## [14.3.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.3.0...v14.3.1) (2020-05-07)


### Bug Fixes

* **filters:** Correctly handle filters without v-model ([8682b6c](https://gitlab.com/gitlab-org/gitlab-ui/commit/8682b6cb405caa768c45a93872e2da820821e750))
* **GlSearchBoxByType:** Fix position of clear input icon ([dd5bacc](https://gitlab.com/gitlab-org/gitlab-ui/commit/dd5baccee5df7b51d5f039384d2d8f6dcd01ee7d))

# [14.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.2.1...v14.3.0) (2020-05-07)


### Features

* **filters:** Make filters component to follow design system ([20b8fbd](https://gitlab.com/gitlab-org/gitlab-ui/commit/20b8fbdd3f389fd36f0df946910402bdc67b0cb0))

## [14.2.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.2.0...v14.2.1) (2020-05-06)


### Bug Fixes

* **css:** Button text ellipsis ([e0368cd](https://gitlab.com/gitlab-org/gitlab-ui/commit/e0368cd90d0450d3245bf06be89c6ab99051b11d))

# [14.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.1.0...v14.2.0) (2020-05-06)


### Features

* **GlNewDropdown:** Add inherited toggle-class ([50243de](https://gitlab.com/gitlab-org/gitlab-ui/commit/50243deb8c60631ff189a870152b6655bc41b0ee))

# [14.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v14.0.0...v14.1.0) (2020-05-06)


### Features

* **css:** Add display table utility ([10a378e](https://gitlab.com/gitlab-org/gitlab-ui/commit/10a378e4bf35b1926b2b6d0617e8f15db57c13de))

# [14.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.9.0...v14.0.0) (2020-05-05)


### Bug Fixes

* **css:** update to -10 variable ([485f157](https://gitlab.com/gitlab-org/gitlab-ui/commit/485f157f9f3268a978a41c4ae740c659b86c3092))
* Rename gray-0 to gray-10 ([13905a6](https://gitlab.com/gitlab-org/gitlab-ui/commit/13905a680cfcbbb70b2ea1b88adc004ad61cb310))


### Features

* **Path:** Add metric variant ([e34938d](https://gitlab.com/gitlab-org/gitlab-ui/commit/e34938d6c28e83e956aa2575368667adb2596cdc))


### BREAKING CHANGES

* $white is $gray-0, whereas this is still slightly gray

# [13.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.8.2...v13.9.0) (2020-05-04)


### Features

* **path:** Implement path component ([82f36c4](https://gitlab.com/gitlab-org/gitlab-ui/commit/82f36c4fa75f3b428a569a76d85e88be655c0a73))

## [13.8.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.8.1...v13.8.2) (2020-04-30)


### Bug Fixes

* Update card component border and footer colors ([26047e7](https://gitlab.com/gitlab-org/gitlab-ui/commit/26047e74a88c81c0d8d3a40945af76b02b75c3bd))

## [13.8.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.8.0...v13.8.1) (2020-04-30)


### Bug Fixes

* **GlButton:** update loading style ([a03ad7c](https://gitlab.com/gitlab-org/gitlab-ui/commit/a03ad7c16936a91b9a80fbf7bf2ead62a64a3422))

# [13.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.7.1...v13.8.0) (2020-04-30)


### Features

* **label:** Remove doc link from scoped labels and correct padding ([b4ae184](https://gitlab.com/gitlab-org/gitlab-ui/commit/b4ae18490938d27c2c0cf0f7832a2f547e686181))

## [13.7.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.7.0...v13.7.1) (2020-04-30)


### Bug Fixes

* **deps:** upgrade Vue and move it out of dependencies ([55c87d5](https://gitlab.com/gitlab-org/gitlab-ui/commit/55c87d51bcf9ad8dca85a3002def47b80af6217f))
* **filters:** Use dropdown icons in filters ([21b4619](https://gitlab.com/gitlab-org/gitlab-ui/commit/21b4619baf9ab3c2153ad06000ce42f908385a5e))

# [13.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.6.1...v13.7.0) (2020-04-30)


### Bug Fixes

* **GlDatepicker:** fix datepicker positioning ([4263493](https://gitlab.com/gitlab-org/gitlab-ui/commit/42634936e88b29dac9995f89d52c2ef7f581f523))


### Features

* **GlDatepicker:** add slot for custom input ([95bb120](https://gitlab.com/gitlab-org/gitlab-ui/commit/95bb120a664efe69d41c74f3ae6dacd821bb8a26))

## [13.6.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.6.0...v13.6.1) (2020-04-29)


### Bug Fixes

* **css:** Support arbitrary widths on dropdown ([2ffe961](https://gitlab.com/gitlab-org/gitlab-ui/commit/2ffe96169e94b11b532ba16c6673c976876b2c52))
* **GlTextArea:** Set SSOT for styles ([62f703c](https://gitlab.com/gitlab-org/gitlab-ui/commit/62f703cd2f942fffe290a091defee437a2a8ae64))

# [13.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.5.0...v13.6.0) (2020-04-29)


### Features

* Add annotation markers to gitlab ui ([1d0c55e](https://gitlab.com/gitlab-org/gitlab-ui/commit/1d0c55eeff99d71a66dab9e9887d0096c4e74b3a))

# [13.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.4.0...v13.5.0) (2020-04-27)


### Features

* **css:** Remove white-light variable ([24669c2](https://gitlab.com/gitlab-org/gitlab-ui/commit/24669c2a2ea6e3c46a560a9dc178ba74a25e1438))

# [13.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.3.0...v13.4.0) (2020-04-24)


### Features

* **css:** Add line-height and text-align utils ([3048229](https://gitlab.com/gitlab-org/gitlab-ui/commit/3048229463886cf829a07b787c4e722f47d609e8))

# [13.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.2.0...v13.3.0) (2020-04-24)


### Features

* **nav:** Implement GlNav, GlNavItem components ([962b31b](https://gitlab.com/gitlab-org/gitlab-ui/commit/962b31b913aed57320092400505d2714266c94f6))

# [13.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.1.0...v13.2.0) (2020-04-23)


### Bug Fixes

* **button:** disable loading button ([111349d](https://gitlab.com/gitlab-org/gitlab-ui/commit/111349d7dc8f2d72b86b8f4410d510ec2ec7bcea))


### Features

* **button:** add loading button ([3d5f2f2](https://gitlab.com/gitlab-org/gitlab-ui/commit/3d5f2f26f408816dd69d52bce28b4b0a447e0d29))

# [13.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v13.0.0...v13.1.0) (2020-04-22)


### Features

* **GlFormCheckboxTree:** add toggle all option ([15ce0fd](https://gitlab.com/gitlab-org/gitlab-ui/commit/15ce0fd113a11f8cdb1ac6515ef76025f246a275))

# [13.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.3.0...v13.0.0) (2020-04-21)


### Bug Fixes

* **input:** visible border in iOS ([d9a5aba](https://gitlab.com/gitlab-org/gitlab-ui/commit/d9a5abaf8808ca265ccb489ad69206e435e9eb4d))


### Code Refactoring

* 💡 Remove deprecated number sizes from GlLoadingIcon ([d26f397](https://gitlab.com/gitlab-org/gitlab-ui/commit/d26f397a078f07a2d0a3a5e0085acded2a2cd9f2))


### BREAKING CHANGES

* 🧨 GlLoadingIcon does not accept size as a number.
Use string instead.

# [12.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.2.0...v12.3.0) (2020-04-21)


### Features

* add clearInput event ([21b9656](https://gitlab.com/gitlab-org/gitlab-ui/commit/21b965621b4fdf8f2310c2c434c625c6d486e1d7))

# [12.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.1.1...v12.2.0) (2020-04-20)


### Features

* **markdown:** Implement markdown typescale ([05d4a64](https://gitlab.com/gitlab-org/gitlab-ui/commit/05d4a64fe30f20937aa0eeed6ed513fb2ddbac57))

## [12.1.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.1.0...v12.1.1) (2020-04-18)


### Bug Fixes

* **charts:** update legend label spacing ([1a42ccc](https://gitlab.com/gitlab-org/gitlab-ui/commit/1a42ccc3f49af871f0393560939bed4bccfe496d))

# [12.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.0.3...v12.1.0) (2020-04-17)


### Features

* **heatmap:** Add tooltip slot ([0f7f74e](https://gitlab.com/gitlab-org/gitlab-ui/commit/0f7f74eca1b8b96ff2af8ba6614a9367570897cf))

## [12.0.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.0.2...v12.0.3) (2020-04-16)


### Bug Fixes

* **dropdown:** Remove bootstrap [@extend](https://gitlab.com/extend) statements ([ff4fa76](https://gitlab.com/gitlab-org/gitlab-ui/commit/ff4fa76b3a83cacefafc01f1a401aa7bbdb21265))

## [12.0.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.0.1...v12.0.2) (2020-04-16)


### Bug Fixes

* Resolve pagination links not working in compact mode ([978bb49](https://gitlab.com/gitlab-org/gitlab-ui/commit/978bb49338928b7343ee4083d513824780ea9ac3))

## [12.0.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v12.0.0...v12.0.1) (2020-04-16)


### Bug Fixes

* **charts:** fix engineering notation formatter locale issue ([20259fb](https://gitlab.com/gitlab-org/gitlab-ui/commit/20259fb1481ce627ddad764c835643883d75426d))
* **GlFormTextArea:** Apply error styles ([79bac04](https://gitlab.com/gitlab-org/gitlab-ui/commit/79bac04cc744d0a950938e95afb93b105a566ef4))

# [12.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.7.0...v12.0.0) (2020-04-15)


### Code Refactoring

* rename GlNewButton to GlButton ([abc8156](https://gitlab.com/gitlab-org/gitlab-ui/commit/abc81566df72c691fbe34839d62e484c29d11f12))


### BREAKING CHANGES

* The GlNewButton component has been renamed to GlButton

Make sure to update your import statements:

```patch
- import { GlNewButton } from '@gitlab/ui';
+ import { GlButton } from '@gitlab/ui';
```

And your templates:

```patch
- <gl-new-button></gl-new-button>
+ <gl-button></gl-button>
```

# [11.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.6.0...v11.7.0) (2020-04-14)


### Features

* add GlFormCheckboxTree component ([5818d56](https://gitlab.com/gitlab-org/gitlab-ui/commit/5818d564eaf3dcb03f710ade22741a5cf7c7e519))

# [11.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.5.0...v11.6.0) (2020-04-14)


### Features

* Add annotations arrow and tooltip ([56d8a41](https://gitlab.com/gitlab-org/gitlab-ui/commit/56d8a411dce5af6dcee4642b7b6033d31365b3a3))

# [11.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.4.0...v11.5.0) (2020-04-14)


### Features

* **css:** Add display utilities ([5cf8173](https://gitlab.com/gitlab-org/gitlab-ui/commit/5cf81733bb2188d66097c4aa4ce39926ba2dd6ae))
* **css:** Add opacity utilities ([2ce766a](https://gitlab.com/gitlab-org/gitlab-ui/commit/2ce766a29522f0924e99b5f27aaf5ef2974f35c7))

# [11.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.3.0...v11.4.0) (2020-04-14)


### Features

* **css:** Add missing scale utilities ([f21d085](https://gitlab.com/gitlab-org/gitlab-ui/commit/f21d0851c0d7a8b4ae383873de6d95e8a15afeed))

# [11.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.2.1...v11.3.0) (2020-04-13)


### Features

* **charts:** Update chart css for long labels ([41bef73](https://gitlab.com/gitlab-org/gitlab-ui/commit/41bef73421abb8d220a96a51bcb881bd52ad8d2c))
* **css:** Add flex fill css utility ([65ba334](https://gitlab.com/gitlab-org/gitlab-ui/commit/65ba334f8ff45f0ce26d71aff738409c4fca7866))
* **css:** Add padding utilities ([4d65503](https://gitlab.com/gitlab-org/gitlab-ui/commit/4d655037824a838899c3599c484a40e08837811a))

## [11.2.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.2.0...v11.2.1) (2020-04-09)


### Bug Fixes

* **button:** icon button fill color ([e913662](https://gitlab.com/gitlab-org/gitlab-ui/commit/e9136627b454ddd87ba71a3f15d5095bd3938e07))

# [11.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.1.0...v11.2.0) (2020-04-08)


### Features

* **css:** Add cursor grab css utility ([ad0f02c](https://gitlab.com/gitlab-org/gitlab-ui/commit/ad0f02c61ba3571c5f96d443c2d6ae0418b1d02b))
* **css:** Update transform naming ([c8621ca](https://gitlab.com/gitlab-org/gitlab-ui/commit/c8621ca7b2530050afd19870b96169676510126e))

# [11.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.0.3...v11.1.0) (2020-04-06)


### Features

* **css:** Add missing y transform utilities ([32f8b26](https://gitlab.com/gitlab-org/gitlab-ui/commit/32f8b26f04af049dc947c23a3f4162fe1772cf1c))

## [11.0.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.0.2...v11.0.3) (2020-04-06)


### Bug Fixes

* **filters:** Fix styles for GitLab ([2fb56b4](https://gitlab.com/gitlab-org/gitlab-ui/commit/2fb56b4c7ad9f5623f5ece39105789a1e5e19c44))

## [11.0.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.0.1...v11.0.2) (2020-04-03)


### Bug Fixes

* **DaterangePicker:** Add sameDaySelection prop ([44a6459](https://gitlab.com/gitlab-org/gitlab-ui/commit/44a645972dc998bfd05b85909cfdd0ff7f95d305))

## [11.0.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v11.0.0...v11.0.1) (2020-04-03)


### Bug Fixes

* Chart tooltip positioning logic ([03d7dbf](https://gitlab.com/gitlab-org/gitlab-ui/commit/03d7dbf268a9df4c9f4716ba2664c793b083adcc))

# [11.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.4.0...v11.0.0) (2020-04-02)


### Features

* Add focus state mixins, utilize for form inputs ([72c7be1](https://gitlab.com/gitlab-org/gitlab-ui/commit/72c7be11f753eae566e5bd45c1d4faafb18027b1))
* rename GlButton to GlDeprecatedButton ([123324b](https://gitlab.com/gitlab-org/gitlab-ui/commit/123324bb5f973456d233741b4c93c9b1e885579e))


### BREAKING CHANGES

* The GlButton component has been renamed to
GlDeprecatedButton.

Import statements need to be updated accordingly:

Before:

import { GlButton } from '@gitlab/ui';

After:

import { GlDeprecatedButton } from '@gitlab/ui';

In Vue templates, make sure to update the component's usages as well:

Before:

<gl-button>My button</gl-button>

After:

<gl-deprecated-button>My button</gl-deprecated-button>

# [10.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.3.3...v10.4.0) (2020-04-02)


### Features

* **colors:** Add more text colors for export ([34e39e3](https://gitlab.com/gitlab-org/gitlab-ui/commit/34e39e3ac1dc6b72cc92a3e9bb9f2423b650fe62))

## [10.3.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.3.2...v10.3.3) (2020-04-01)


### Bug Fixes

* Add missing GlAvatar import in GlNewDropdownItem ([be6b3fe](https://gitlab.com/gitlab-org/gitlab-ui/commit/be6b3fe638ee0aeb926ade2d0d7c35500328d249))
* **banner:** set line heights and spacings explicitly ([0dbb214](https://gitlab.com/gitlab-org/gitlab-ui/commit/0dbb214774b1ae356b05898c07de97822f7b6d84))

## [10.3.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.3.1...v10.3.2) (2020-04-01)


### Bug Fixes

* block button padding ([e2bc974](https://gitlab.com/gitlab-org/gitlab-ui/commit/e2bc974a24181c7f1cd3cda5f854aab00beaf1ea))
* **button:** center text in block button ([b4d10b7](https://gitlab.com/gitlab-org/gitlab-ui/commit/b4d10b7527c85828645e75b2df3cb7588049c4ba))

## [10.3.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.3.0...v10.3.1) (2020-04-01)


### Bug Fixes

* **radio:** Fix v-model binding ([611e369](https://gitlab.com/gitlab-org/gitlab-ui/commit/611e369c8bffbd9620da7ce5360c16b44dd5fdce))

# [10.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.2.0...v10.3.0) (2020-04-01)


### Features

* **filters:** Fill operator part if only one operator exists ([be4ee6f](https://gitlab.com/gitlab-org/gitlab-ui/commit/be4ee6f81849c34d02642b136a3acb5ed9c1f180))

# [10.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.1.3...v10.2.0) (2020-04-01)


### Features

* Add annotations ([24126fc](https://gitlab.com/gitlab-org/gitlab-ui/commit/24126fcd17ec616bc158c29726049c5d0bafc47e))

## [10.1.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.1.2...v10.1.3) (2020-03-31)


### Bug Fixes

* **segmented_control:** Handle loop edge case ([0afba6f](https://gitlab.com/gitlab-org/gitlab-ui/commit/0afba6fad29e63372b220e401f804ee6b5bd27ad)), closes [/gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1230#note_313466652](https://gitlab.com//gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1230/issues/note_313466652)

## [10.1.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.1.1...v10.1.2) (2020-03-30)


### Bug Fixes

* **form:** Update form group message spacing ([a82db41](https://gitlab.com/gitlab-org/gitlab-ui/commit/a82db41e7994019e3e9d444f9a962d8f371d4b5b))

## [10.1.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.1.0...v10.1.1) (2020-03-30)


### Bug Fixes

* align dropdown caret when block ([eefffcd](https://gitlab.com/gitlab-org/gitlab-ui/commit/eefffcda51bb6c1d722459d88fbd4fac0a6a6189))
* use gitlab util class ([7a174f6](https://gitlab.com/gitlab-org/gitlab-ui/commit/7a174f688f2c4fe3d8cf36e78d1171256ad1c8ec))

# [10.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.0.1...v10.1.0) (2020-03-27)


### Features

* **colors:** Mark color variables as default ([e9b0d5b](https://gitlab.com/gitlab-org/gitlab-ui/commit/e9b0d5bb6ff86eabb2748b0b339a721af5d73999))


### Reverts

* only run on danger-review MR creation/update ([fa68ce0](https://gitlab.com/gitlab-org/gitlab-ui/commit/fa68ce03c723205f6b11c48ed9c4c8372b98de1e))

## [10.0.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v10.0.0...v10.0.1) (2020-03-25)


### Bug Fixes

* **popover:** add margin 0 by default for popover header ([0028f6f](https://gitlab.com/gitlab-org/gitlab-ui/commit/0028f6f3672d5eec951c895f772c6f231be51aba))

# [10.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.33.1...v10.0.0) (2020-03-23)


### Features

* **colors:** Improve color palette accessibility ([8309567](https://gitlab.com/gitlab-org/gitlab-ui/commit/8309567ffb15bc01011abaf9450178f49ae4ad83))


### Reverts

* only run on danger-review MR creation/update ([3d174e5](https://gitlab.com/gitlab-org/gitlab-ui/commit/3d174e550e6a5b3f02790932d8c0cb78591f77ab))


### BREAKING CHANGES

* **colors:** Update color variables to match the
accessibility improvements in the Pajamas color specification

## [9.33.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.33.0...v9.33.1) (2020-03-23)


### Bug Fixes

* Make data visualization variables clearer ([5ca173a](https://gitlab.com/gitlab-org/gitlab-ui/commit/5ca173abd7bb3f268dd1567a7c64810d7de8765d))

# [9.33.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.32.0...v9.33.0) (2020-03-20)


### Features

* Data visualization color palette ([22a979f](https://gitlab.com/gitlab-org/gitlab-ui/commit/22a979f8a1c46ab2dca41084e176fb64b3c9bf25))

# [9.32.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.31.2...v9.32.0) (2020-03-19)


### Features

* Dynamically position axis names for long axis labels ([466bbf5](https://gitlab.com/gitlab-org/gitlab-ui/commit/466bbf5e914c8c4cbc922bd7f74fc083aad5af62))

## [9.31.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.31.1...v9.31.2) (2020-03-19)


### Bug Fixes

* add test coverage for events ([4f3a3a5](https://gitlab.com/gitlab-org/gitlab-ui/commit/4f3a3a5aa78b51f73f7bde6d8c89c059820b0b14))
* adjust secondary action on modal ([52079c7](https://gitlab.com/gitlab-org/gitlab-ui/commit/52079c79896d10b5d4502bc7afeed27ad07b105d))

## [9.31.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.31.0...v9.31.1) (2020-03-19)


### Bug Fixes

* Set label font size ([8241e2a](https://gitlab.com/gitlab-org/gitlab-ui/commit/8241e2ac527bbfa65b3d82d49cf284371c34b179))

# [9.31.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.30.0...v9.31.0) (2020-03-18)


### Features

* **css:** Generate stateful utility classes ([e9ca4f1](https://gitlab.com/gitlab-org/gitlab-ui/commit/e9ca4f180ea7fd5aa2b3afce09cf283f8d4ca786))

# [9.30.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.29.0...v9.30.0) (2020-03-17)


### Features

* **barchart:** Support bar charts ([d4d0f7e](https://gitlab.com/gitlab-org/gitlab-ui/commit/d4d0f7e9206f27be46fb0fcda1665dad8fb2c35e))

# [9.29.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.28.0...v9.29.0) (2020-03-17)


### Features

* **css:** Add missing spacing utilities ([bd24d60](https://gitlab.com/gitlab-org/gitlab-ui/commit/bd24d601aff43a43ab18ce5bd90d72529cec2144))

# [9.28.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.27.0...v9.28.0) (2020-03-14)


### Features

* Receive inherited attrs & listeners in infinite scroll ([2847f92](https://gitlab.com/gitlab-org/gitlab-ui/commit/2847f9239d4496dd89b3b74cd5f4edbc5c0f59e1))

# [9.27.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.26.0...v9.27.0) (2020-03-13)


### Features

* **filters:** Fix error when scrolling to suggestion ([4595b2b](https://gitlab.com/gitlab-org/gitlab-ui/commit/4595b2bd2f5496bcef1dd51d7f62f1bd3c314d55))

# [9.26.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.25.0...v9.26.0) (2020-03-13)


### Features

* **css:** Add spacing utilities ([1f03cfe](https://gitlab.com/gitlab-org/gitlab-ui/commit/1f03cfe64b41b788c9fd94df9f3ad8884f31b741))

# [9.25.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.24.0...v9.25.0) (2020-03-13)


### Features

* **css:** add spacing utilities ([56e534c](https://gitlab.com/gitlab-org/gitlab-ui/commit/56e534cbcb4d062b48470149a1c8e8dc2f97a129))

# [9.24.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.23.2...v9.24.0) (2020-03-12)


### Features

* **filters:** Sync filters implementation with draft spec ([436eb51](https://gitlab.com/gitlab-org/gitlab-ui/commit/436eb5139e90ec4a096e033454553ca81eee6348))
* **utilities:** Add missing utilities ([fd029b2](https://gitlab.com/gitlab-org/gitlab-ui/commit/fd029b28aba2fec62453e3f01f6cc658ec0cf1fc))

## [9.23.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.23.1...v9.23.2) (2020-03-11)


### Bug Fixes

* Update pagination scss to remove custom bootstrap breakpoint ([adc5c55](https://gitlab.com/gitlab-org/gitlab-ui/commit/adc5c550e82177bd02cd5b42e5e2a434cd32eaef))

## [9.23.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.23.0...v9.23.1) (2020-03-10)


### Bug Fixes

* add gl-border-gray-100 utility ([bd2897c](https://gitlab.com/gitlab-org/gitlab-ui/commit/bd2897cc499dde7edf08838300ab1413a0971660))

# [9.23.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.22.1...v9.23.0) (2020-03-05)


### Features

* Adds an navbar component ([3c3d97e](https://gitlab.com/gitlab-org/gitlab-ui/commit/3c3d97e4904b41732aaa355095f6f421d5e03cc6))

## [9.22.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.22.0...v9.22.1) (2020-03-04)


### Bug Fixes

* Reverting flag until it’s true ([21139f0](https://gitlab.com/gitlab-org/gitlab-ui/commit/21139f09f1dfcf4e5e8529b0075f84b089bcb26e))

# [9.22.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.21.2...v9.22.0) (2020-03-04)


### Bug Fixes

* **rollup:** include config.js in dist/ ([d0792d8](https://gitlab.com/gitlab-org/gitlab-ui/commit/d0792d8b4857dc7424c941ef3b5ca119c74c12ab))


### Features

* Adds an observer component ([314dda4](https://gitlab.com/gitlab-org/gitlab-ui/commit/314dda4b7e37ba103daaa797c37a718d504a5b3c))

## [9.21.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.21.1...v9.21.2) (2020-03-04)


### Bug Fixes

* Registers GlIcon for the GlNewDropdownItems component ([91ae2a3](https://gitlab.com/gitlab-org/gitlab-ui/commit/91ae2a30ab78d8bebdd5989f39655e07f166cfac))

## [9.21.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.21.0...v9.21.1) (2020-03-03)


### Bug Fixes

* **label:** Style fix for labels without link ([0807e47](https://gitlab.com/gitlab-org/gitlab-ui/commit/0807e472f92b423b0894b33e5b336fa54959ad55))

# [9.21.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.20.0...v9.21.0) (2020-03-03)


### Features

* Allow input examples to display ([496f979](https://gitlab.com/gitlab-org/gitlab-ui/commit/496f9798d3e9c926851aaa5ce03f916cd2f1a7ea))

# [9.20.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.19.0...v9.20.0) (2020-03-02)


### Features

* **css:** add 'gl-pl-4' spacing mixin ([7ff8bab](https://gitlab.com/gitlab-org/gitlab-ui/commit/7ff8babf2d1d2278724e2d19f9b661293c7757a7))

# [9.19.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.18.2...v9.19.0) (2020-03-02)


### Features

* **token:** Add new token variants ([603e909](https://gitlab.com/gitlab-org/gitlab-ui/commit/603e909752598ebcd144873dca575ecb1b645b32))

## [9.18.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.18.1...v9.18.2) (2020-03-02)


### Bug Fixes

* Add border to invalid input ([e245506](https://gitlab.com/gitlab-org/gitlab-ui/commit/e245506e2f45a7354a207de32103d58afc0d7b27))

## [9.18.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.18.0...v9.18.1) (2020-02-29)


### Bug Fixes

* **avatar:** Fix avatar styles SASS error ([5df627a](https://gitlab.com/gitlab-org/gitlab-ui/commit/5df627a5b13d27c71d9135264d55405f23ca992a))

# [9.18.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.17.0...v9.18.0) (2020-02-28)


### Features

* Make gitlab-ui tree-shakeable ([6fd7f1f](https://gitlab.com/gitlab-org/gitlab-ui/commit/6fd7f1fdc12d731d87f75148a1688fba18a3d974))

# [9.17.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.16.0...v9.17.0) (2020-02-26)


### Features

* **avatar:** Use correct avatar font sizes ([e6a149a](https://gitlab.com/gitlab-org/gitlab-ui/commit/e6a149a986782252e911384cd7c8d073f8769abf))

# [9.16.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.15.0...v9.16.0) (2020-02-25)


### Features

* **typescale:** Implement UI typescale ([4fc0e56](https://gitlab.com/gitlab-org/gitlab-ui/commit/4fc0e56217800069bf85a4a2e25b4acd2047bfb9))

# [9.15.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.14.0...v9.15.0) (2020-02-25)


### Features

* update tooltip delay speed ([f127fe2](https://gitlab.com/gitlab-org/gitlab-ui/commit/f127fe2979a1b4381afeaf000ff97bf83f378ae6))

# [9.14.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.13.0...v9.14.0) (2020-02-24)


### Features

* **pagination:** use gl-icon in prev and next page items ([6d42aff](https://gitlab.com/gitlab-org/gitlab-ui/commit/6d42aff7beb73a7b4adc065cb7fb86f918da73d6))

# [9.13.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.12.0...v9.13.0) (2020-02-24)


### Features

* **label:** Add title to scoped label tooltip ([47d96d5](https://gitlab.com/gitlab-org/gitlab-ui/commit/47d96d5a70e412f62d3f67ba60af98ee3c6d9fad))

# [9.12.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.11.2...v9.12.0) (2020-02-24)


### Features

* Adds a new button variation ([a9f95c9](https://gitlab.com/gitlab-org/gitlab-ui/commit/a9f95c91d4b1d4079ba4236ea5b055346b5fd99f))

## [9.11.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.11.1...v9.11.2) (2020-02-19)


### Bug Fixes

* **dropdown:** Fix dropdown caret alignment ([fc9126c](https://gitlab.com/gitlab-org/gitlab-ui/commit/fc9126c591bc61f09ab907f1b688c21e56cfd355))

## [9.11.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.11.0...v9.11.1) (2020-02-17)


### Bug Fixes

* Show all tooltips when multiple series ([ed022dc](https://gitlab.com/gitlab-org/gitlab-ui/commit/ed022dc036dba2629907a002f898df43e8ed2d84))

# [9.11.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.10.1...v9.11.0) (2020-02-17)


### Features

* **css:** Add pajamas typescale variables ([bd9ed44](https://gitlab.com/gitlab-org/gitlab-ui/commit/bd9ed447d81c790b488ddb0364fbdd4fca72e241))

## [9.10.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.10.0...v9.10.1) (2020-02-14)


### Bug Fixes

* **radio:** Explicitly set line height ([7d58f43](https://gitlab.com/gitlab-org/gitlab-ui/commit/7d58f438e530cc08a051273dc0a35bcb68c84aac))

# [9.10.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.9.0...v9.10.0) (2020-02-13)


### Features

* **css:** Expose gl-bg-theme-indigo-50 utility class ([3a364e2](https://gitlab.com/gitlab-org/gitlab-ui/commit/3a364e25fe18c915642d6458f5318ae401c629d5))

# [9.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.8.0...v9.9.0) (2020-02-12)


### Features

* Add autofocus in modal component ([a1f0c8d](https://gitlab.com/gitlab-org/gitlab-ui/commit/a1f0c8dc30de6f525925cae8ad32897a434d42d9))

# [9.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.7.0...v9.8.0) (2020-02-11)


### Features

* Add tooltip to badge for GlAvatarsInline ([81c5495](https://gitlab.com/gitlab-org/gitlab-ui/commit/81c5495852a38fea8bc920e74700aea8c9035105))

# [9.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.6.1...v9.7.0) (2020-02-11)


### Features

* Add gl-form component ([1c4aa0f](https://gitlab.com/gitlab-org/gitlab-ui/commit/1c4aa0f392844bf5715e7335baa688a2b831def9))

## [9.6.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.6.0...v9.6.1) (2020-02-11)


### Bug Fixes

* Bind bootstrap-vue tooltip events ([bee4bec](https://gitlab.com/gitlab-org/gitlab-ui/commit/bee4bec39fca86c667fcf5c1f1a60985459d05d8))

# [9.6.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.5.0...v9.6.0) (2020-02-10)


### Features

* add visible label to gl-toggle ([d57be1e](https://gitlab.com/gitlab-org/gitlab-ui/commit/d57be1eccb5c5b19702d30c650f0e0f97cfc2c22))

# [9.5.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.4.1...v9.5.0) (2020-02-07)


### Features

* **filtered_search:** Implement filtered search ([7ff1ee2](https://gitlab.com/gitlab-org/gitlab-ui/commit/7ff1ee2f1d847b002bad215fdfc03a0b088de507))

## [9.4.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.4.0...v9.4.1) (2020-02-07)


### Bug Fixes

* **form-input:** apply form-control-focus mixin ([5f2f686](https://gitlab.com/gitlab-org/gitlab-ui/commit/5f2f6860348a499879b3ce925b82a773cfe79904))

# [9.4.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.3.0...v9.4.0) (2020-02-05)


### Bug Fixes

* **labels:** moved ref from link ([7bc45d0](https://gitlab.com/gitlab-org/gitlab-ui/commit/7bc45d03734814d1d7fc255585a7dec09a240b1e))


### Features

* **carousel:** Build carousel component ([f29e8fe](https://gitlab.com/gitlab-org/gitlab-ui/commit/f29e8fe0a8d1a0e72a3555b37697058a29068350))

# [9.3.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.6...v9.3.0) (2020-02-04)


### Features

* update size prop to match ds ([ec1400f](https://gitlab.com/gitlab-org/gitlab-ui/commit/ec1400f49f311463919630da6015b8ea0a8f60ef))

## [9.2.6](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.5...v9.2.6) (2020-01-31)


### Bug Fixes

* Make classes for gl-filtered-search-suggestion more specific in order to override styles in GitLab ([d475389](https://gitlab.com/gitlab-org/gitlab-ui/commit/d4753891600e6c7ba89f45581b757b207f09fb44))

## [9.2.5](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.4...v9.2.5) (2020-01-31)


### Reverts

* fix(bootstrap-vue): Upgrade bootstrap-vue ([f20c5e1](https://gitlab.com/gitlab-org/gitlab-ui/commit/f20c5e1006fe95e5687621956d6d15fc776a6c3d))

## [9.2.4](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.3...v9.2.4) (2020-01-30)


### Bug Fixes

* **bootstrap-vue:** Upgrade bootstrap-vue to 2.3.0 ([d531986](https://gitlab.com/gitlab-org/gitlab-ui/commit/d5319865534f6e496f2255b1d3591352c6e5bb60))

## [9.2.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.2...v9.2.3) (2020-01-29)


### Bug Fixes

* add @gitlab/svgs to peerDependencies ([00381c2](https://gitlab.com/gitlab-org/gitlab-ui/commit/00381c285d557142e4569b7d4e03d28478896237))

## [9.2.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.1...v9.2.2) (2020-01-28)


### Bug Fixes

* Help text font size being overridden ([3bb7d41](https://gitlab.com/gitlab-org/gitlab-ui/commit/3bb7d4199edf93999a3a46767fe943b3c6a63526))

## [9.2.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.2.0...v9.2.1) (2020-01-28)


### Bug Fixes

* **popover:** Expose popover event listeners ([684c786](https://gitlab.com/gitlab-org/gitlab-ui/commit/684c7862bdac3f38edf05ba5ef2dcc533202b44d))

# [9.2.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.1.0...v9.2.0) (2020-01-28)


### Features

* **filtered_search:** Implement static binary token ([591ced0](https://gitlab.com/gitlab-org/gitlab-ui/commit/591ced08a21be587cf8ac7ac3395d859c2721511))

# [9.1.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.0.2...v9.1.0) (2020-01-28)


### Features

* Add Collapse component ([6be57d0](https://gitlab.com/gitlab-org/gitlab-ui/commit/6be57d07628b717b65f4df51a477ca577d2d068a))
* **sprintf:** Allow full sentence interpolation ([af82918](https://gitlab.com/gitlab-org/gitlab-ui/commit/af82918633eaf19d40ea2da36e98401496f6e27a)), closes [/gitlab.com/gitlab-org/gitlab/issues/21344#note_221778896](https://gitlab.com//gitlab.com/gitlab-org/gitlab/issues/21344/issues/note_221778896)

## [9.0.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.0.1...v9.0.2) (2020-01-24)


### Bug Fixes

* **broadcast message:** adjust vertical spacing ([82d5cca](https://gitlab.com/gitlab-org/gitlab-ui/commit/82d5cca46526f6c2118e113edca9ea04e173b432))

## [9.0.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v9.0.0...v9.0.1) (2020-01-24)


### Bug Fixes

* Enhance daterange_picker range selection ([fa2c4c1](https://gitlab.com/gitlab-org/gitlab-ui/commit/fa2c4c1cbc0fbc4c3b4744c31b63470322f120e7))

# [9.0.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.21.0...v9.0.0) (2020-01-23)


### Code Refactoring

* Upgrade BootstrapVue to 2.1.0 ([6d6ef5b](https://gitlab.com/gitlab-org/gitlab-ui/commit/6d6ef5bfaade2dc13f86f5f27311aaaebef2df69))


### BREAKING CHANGES

* Upgrade bootstrap-vue dependency to version 2.1.0. This
upgrade contains the following breaking changes:

- Import statements for bootstrap-vue components changed.
- BTable component has a new slot syntax for custom content.
- BTabs component has a new slot syntax for contentless tabs
- BPopover and BTooltip components were completely rewritten

For more information about this upgrade, check BootstrapVue changelog
page https://bootstrap-vue.js.org/docs/misc/changelog

# [8.21.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.20.0...v8.21.0) (2020-01-23)


### Features

* **avatars:** Detach collapsed state from badge ([a24270b](https://gitlab.com/gitlab-org/gitlab-ui/commit/a24270bcae6ca70cfe0fb651c9a9aa5349f7933d))

# [8.20.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.19.1...v8.20.0) (2020-01-23)


### Features

* **color:** adding $purple color variables ([9310e3f](https://gitlab.com/gitlab-org/gitlab-ui/commit/9310e3f0d48e32ab3fcfb83cbfe628ced2f95988))

## [8.19.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.19.0...v8.19.1) (2020-01-22)


### Bug Fixes

* **avatars-inline:** Fix collapsible behavior ([bb60fdb](https://gitlab.com/gitlab-org/gitlab-ui/commit/bb60fdb2a8c23982467ccfabfaf85d93ea865d36))

# [8.19.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.18.0...v8.19.0) (2020-01-21)


### Features

* Introduce new modal API ([65d8135](https://gitlab.com/gitlab-org/gitlab-ui/commit/65d81352be7f42256b3f27a52b8b90b5a4a80d6a))

# [8.18.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.17.0...v8.18.0) (2020-01-20)


### Bug Fixes

* Tooltip on Label for gitlab ([10a9507](https://gitlab.com/gitlab-org/gitlab-ui/commit/10a95070d63ad11b51e02eb115a5b92ca120bdd6))


### Features

* update button props ([95f971c](https://gitlab.com/gitlab-org/gitlab-ui/commit/95f971c1be00045769ea91281e1967951efbed76))

# [8.17.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.16.0...v8.17.0) (2020-01-17)


### Features

* **filtered_search:** Implement filtered search binary token ([6b14c21](https://gitlab.com/gitlab-org/gitlab-ui/commit/6b14c21077cf60f601c715baa4b943e2b278327d))

# [8.16.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.15.0...v8.16.0) (2020-01-16)


### Features

* Inline avatars component ([688feba](https://gitlab.com/gitlab-org/gitlab-ui/commit/688febaf7cdb145165042a06a2b2fab8d7ad2f11))

# [8.15.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.14.0...v8.15.0) (2020-01-14)


### Features

* **filtered_search:** Implement filtered search term ([1d0bc82](https://gitlab.com/gitlab-org/gitlab-ui/commit/1d0bc82f3e74159e9572ba96a8af60ab701fc860))

# [8.14.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.13.0...v8.14.0) (2020-01-14)


### Features

* add broadcast message component ([aaf98e6](https://gitlab.com/gitlab-org/gitlab-ui/commit/aaf98e65aa01bc3607069eaa9b0d3209f02e680e))

# [8.13.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.12.0...v8.13.0) (2020-01-10)


### Features

* **filtered_search:** Implement filtered search suggestions ([8b67b12](https://gitlab.com/gitlab-org/gitlab-ui/commit/8b67b12313df23a3b576f4f6f5a8d7a95d682631))

# [8.12.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.11.0...v8.12.0) (2020-01-10)


### Features

* Add Input Group component ([16fa807](https://gitlab.com/gitlab-org/gitlab-ui/commit/16fa807113dc0faf847577e5e0b8953e3d940537))

# [8.11.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.10.0...v8.11.0) (2020-01-09)


### Bug Fixes

* prevent text prop when icon/split are used ([ad5e9ec](https://gitlab.com/gitlab-org/gitlab-ui/commit/ad5e9ec59c23d89638d5552558ec3edb91b96f5d))
* Set current year to fixed value ([789b612](https://gitlab.com/gitlab-org/gitlab-ui/commit/789b612487a987f5be2f45449461b2596b804812))


### Features

* Add icon and split to new dropdown ([b9dc446](https://gitlab.com/gitlab-org/gitlab-ui/commit/b9dc446c040e00008d47df28babf629e33f5a404))
* **search:** Add tooltip container support for search components ([2ce9b7c](https://gitlab.com/gitlab-org/gitlab-ui/commit/2ce9b7c22e8edf152ea1a67101e3d6dd585c894e))

# [8.10.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.9.0...v8.10.0) (2019-12-30)


### Features

* Export breakpoints from utils ([b6e4ace](https://gitlab.com/gitlab-org/gitlab-ui/commit/b6e4ace54fec274e0de213c030eb42af1b0306a4))

# [8.9.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.8.1...v8.9.0) (2019-12-20)


### Features

* update dropdown examples ([ba6d9d7](https://gitlab.com/gitlab-org/gitlab-ui/commit/ba6d9d7b52bad22687db9f95b97eb06e37aebb8a))

## [8.8.1](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.8.0...v8.8.1) (2019-12-20)


### Bug Fixes

* Fix empty tooltip on sparklines ([a74c6e1](https://gitlab.com/gitlab-org/gitlab-ui/commit/a74c6e169d73b1ca15e64ed23a0a557ebae9fda6))

# [8.8.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.7.0...v8.8.0) (2019-12-17)


### Features

* Add xported utils ([1d35af2](https://gitlab.com/gitlab-org/gitlab-ui/commit/1d35af230a741503162e1f295e9737fcac597d4c))

# [8.7.0](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.3...v8.7.0) (2019-12-17)


### Features

* Init card component ([783121d](https://gitlab.com/gitlab-org/gitlab-ui/commit/783121dcb68a7d6d6b28b09ba29869de6610df46))

## [8.6.3](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.2...v8.6.3) (2019-12-16)


### Bug Fixes

* **search:** fix search components with gitlab CSS ([4c65fdd](https://gitlab.com/gitlab-org/gitlab-ui/commit/4c65fdd1485a0eb7f18eca09903309e8f34e60f0))

## [8.6.2](https://gitlab.com/gitlab-org/gitlab-ui/compare/v8.6.1...v8.6.2) (2019-12-13)


### Reverts

* feat: add and style badges ([b78b497](https://gitlab.com/gitlab-org/gitlab-ui/commit/b78b4976d2e47e4d7c37393d460fc134c07edbf6))
